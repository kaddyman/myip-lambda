# myip-lambda 

A lambda triggered by api-gateway that returns your ip address from the event. 
Results are returned in json.

For more information about how I am formatting the event from api-gateway 
please see this terraform module for it.


https://gitlab.com/kaddyman/terraform-modules/-/tree/main/api-gateway-lambda

---

### Build/Deploy

The Makefile can be used to build the zip file and deploy lambda code updates.

---

### bashrc function

Once you have your api up and running you can use something like the below 
in your bash_profile/bashrc to make calls to it.

```bash
function myip(){
  curl -s -X POST -H 'x-api-key: 765rftyg7y8uoihygtf678yihuygtihgyioh3' -H 'Content-Type: application/json' 'https://api.belial.io/ip/prd/' | jq -r '.Item.SourceIp'
}
```

---

### Notes

The API key used is NOT an active key. Only for exmaple. 

