import simplejson as json

def lambda_handler(event, context):
    print(event)
    print(context.__dict__)
    response_dict = {
      "Item" : {
        "SourceIp" : event['headers']['x-forwarded-for']
      }
    }
    return response_dict
